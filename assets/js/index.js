const rightPads = $('.container__box--right--pad');
const rightHeader = $('.container__box__header--right')
const leftHeader = $('.container__box__header--left');
const select2 = $('.controls__block__select');
const endGame = $('#endGame');

class Game {
  constructor(steps = 5) {
    this.steps = steps;
    this.sequenceArray = [];
    this.clickedArray = [];
		this.level = 0;
		this.del = 0;
		this.activePad = false;
		this.isFinished = false;

		/* Events */
    document.getElementById('start').addEventListener('click', this.start.bind(this));
    document.getElementById('reset').addEventListener('click', this.reset.bind(this));

		const pads = [...document.getElementsByClassName('container__box--right--pad')];

		for (let i in pads) {
			pads[i].addEventListener('click', this.onPadClick.bind(this));
		}
  }

  start() {
		this.level = 1;
		this.sequenceArray = [];
    this.clickedArray = [];
		this.del = 0;
		this.activePad = false
		this.isFinished = false;
		$('#start').prop('disabled', true);
		select2.prop('disabled', true);
		endGame.hide();
		rightHeader.removeClass('finished');
		leftHeader.removeClass('finished');
		for (let i = 1; i <= this.steps; i++) {
			const random = (_.floor((_.random(1, true) * 16)) + 1);
			this.sequenceArray.push(random);
		}
		this.activateLed();
	}

  reset() {
		this.sequenceArray = [];
    this.clickedArray = [];
		this.level = 1;
		this.del = 0;
		this.activePad = false
		this.isFinished = false;
		$('#start').prop('disabled', false);
		select2.prop('disabled', false);
		endGame.hide();
		rightPads.removeClass('active');
		rightHeader.html('').removeClass('invalid').removeClass('finished');
		leftHeader.html('').removeClass('finished');
	}


  activateLed() {
		const { level } = this;
		// DISPLAY CURRENT STEPS
		leftHeader.html(`<span>Sequence ${level} of ${this.steps}</span>`);

		_.forEach(this.sequenceArray, (sequence, index) => {
			if (index < level) {
				setTimeout(() => {
					this.highlightLed(`left-${sequence}`);
				}, this.del += 1000);
			}
		});
		setTimeout(() => {
			this.activePad = true;
			rightPads.addClass('active');
			rightHeader.html('').removeClass('invalid');
		}, this.del += 300);
	}

	highlightLed(id) {
		const element = document.getElementById(id);
		element.classList.add('active');
		setTimeout(() => {
			element.classList.remove('active');
		}, 500);
	}

	onPadClick(event) {
			event.preventDefault();
			if (this.activePad && !this.isFinished) {
				const elementId = event.srcElement.id;
				$(`#${elementId}`).addClass("clickedPad")
				setTimeout(function () {
					$(`#${elementId}`).removeClass("clickedPad");
				}, 300);

				let padNumber = _.split(elementId, '-');
				padNumber = _.last(padNumber);
				padNumber = _.parseInt(padNumber);

				this.checkPad(padNumber);
		}
	}

	checkPad(padNumber) {
		this.clickedArray.push(padNumber);
		const clickedArrayLength = _.size(this.clickedArray);
		const clickedArrayIndex = (clickedArrayLength - 1);
		const isEqualArrays = _.isEqual(_.nth(this.clickedArray, clickedArrayIndex), _.nth(this.sequenceArray, clickedArrayIndex));

		// DISPLAY AMOUNT OF CLICKED SEQUENCES ON PADS
		rightHeader.html(`<span>Clicked ${clickedArrayLength} of ${this.level} sequences</span>`);

		// IF CLICKED CORRECT PAD AND SIZE OF CLICKED PADS ARRAY IS EQUAL TO LEVEL
		if (isEqualArrays && _.isEqual(clickedArrayLength, this.level) && !this.isFinished) {
			this.level += 1;
			this.del = 0;
			this.clickedArray = [];
			// IF ALL SEQUENCE DONE - FINISH GAME
			if (_.isEqual(this.steps + 1, this.level)) this.finish();
			// CONTINUE GAME
			else {
				setTimeout(() => {
					this.activePad = false;
					rightPads.removeClass('active');
					rightHeader.html('');
					this.activateLed();
				}, 800);
			}
			// IF CLICKED WRONG PAD
		} else if (!isEqualArrays) {
			this.activePad = false;
			this.del = 0;
			this.clickedArray = [];
			rightHeader.addClass('invalid');
			rightPads.removeClass('active');
			this.activateLed();
		}

	}

	finish() {
		rightPads.removeClass('active');
		rightHeader.addClass('finished');
		leftHeader.addClass('finished');
		$('#start').prop('disabled', false);
		select2.prop('disabled', false);
		endGame.show();
		this.finished = true;
	}

	onStepChange(steps) {
		this.steps = steps;
	}
}

$(document).ready(function() {
	const stepsRange = _.range(1, 11);
	const data =  [];
	_.forEach(stepsRange, step => data.push({ id: step, text: step }));

	select2.each(function() {
		$(this).select2({
			data: ['', ...data],
			placeholder: {
				id: '',
				text: $(this).data('placeholder'),
			},
			allowClear: Boolean($(this).data('allow-clear')),
			width: '100%',
			minimumResultsForSearch: -1,
		})
	});

	const steps = $('#stepsSelect').val(5).trigger('change.select2');
	const newGame = new Game();

	steps.on('change', function() { newGame.onStepChange(_.parseInt($(this).val())) });
})